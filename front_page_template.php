<?php 
	/**
	*		Template Name: Front Page
	*
	*		Supports a short blurb and a list of news and articles posts
	*/
	get_header();
?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="wrap category-intro post" id="post-<?php the_ID(); ?>">
			<?php the_content(); ?>
		</div>
	<?php endwhile; endif; ?>


	<?php query_posts('category_name=news,articles&post_status=publish');?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article <?php post_class('wrap') ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
				<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
			</header>
			<div class="entry">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; else: endif; ?>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

<?php get_footer(); ?>