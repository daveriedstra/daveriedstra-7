<?php 
	/**
	*		Template Name: Web
	*
	*		Supports a short blurb and a loop to show web projects
	*/
	get_header();
?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="wrap category-intro post" id="post-<?php the_ID(); ?>">
			<h1 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
			<?php the_content(); ?>
		</div>
	<?php endwhile; endif; ?>

	<div class="wrap">
		<h1>Projects</h1>
	</div>

	<?php query_posts('post_type=web&post_status=publish');?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article <?php post_class('wrap') ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
				<aside class="meta"><?php the_time('Y') ?></aside>
			</header>
			<div class="entry">
				<?php
					$terms = get_the_terms(get_the_ID(), 'technologies', array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names'));
					$num_terms = count($terms);
					if ($terms !== false && $num_terms > 0):
				?>
				<aside class="technologies">
					<p>
						<small>
							<em>
							<?php
								$i = 0;
								foreach ($terms as $tech) {
									echo $tech->name;
									$i++;
									if ($i < $num_terms)
										echo ', ';
								}
							?>
							</em>
						</small>
					</p>
				</aside>
				<?php 
					endif;
					the_content();
				?>
			</div>
		</article>
	<?php endwhile; else: endif; ?>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

<?php get_footer(); ?>