<?php
	/**
	*		Template Name: Compositions
	*
	*		Supports a short blurb and generates a list of 
	*		the compositions in the dried_compositions theme option
	*
	*/

	get_header(); 

	if (have_posts()) : while (have_posts()) : the_post(); 

	?>	
		<article class="post wrap" id="post-<?php the_ID(); ?>">
			<h1 class="post-title"><?php the_title(); ?></h1>
			<?php the_content(); ?>
	<?php 

	endwhile; endif; 

	/**
	 * renders markup for one project
	 */
	function render_one_project($c) {
		?>
			<div class="composition">
				<h4><?php echo make_proj_name($c); ?> <span class="year">(<?php echo $c['year']; ?>)</span></h4>
				<p class="list-details duration"><?php echo $c['duration']; ?></p>
				<p class="list-details instrumentation"><?php echo $c['instrumentation']; ?></p>
				<p class="list-details description"><?php echo $c['desc']; ?></p>
				<p class="list-details links">
					<?php
						if (isset($c['infoLink']) && strlen($c['infoLink']) > 0)
							echo '<a target="_blank" href="'. $c['infoLink'] .'" title="More information"><i class="fas fa-info-circle"></i></a> ';

						if (isset($c['videoLink']) && strlen($c['videoLink']) > 0)
							echo '<a target="_blank" href="'. $c['videoLink'] .'" title="Video"><i class="fas fa-video"></i></a> ';

						if (isset($c['audioLink']) && strlen($c['audioLink']) > 0)
							echo '<a target="_blank" href="'. $c['audioLink'] .'" title="Audio"><i class="fas fa-headphones"></i></a> ';

						if (isset($c['score']) && strlen($c['score']) > 0)
							echo '<a target="_blank" href="'. wp_get_attachment_url( $c['score'] ) .'" title="Score"><i class="fas fa-file"></i></a>';

						if (isset($c['sourceCodeLink']) && strlen($c['sourceCodeLink']) > 0)
							echo '<a target="_blank" href="'. $c['sourceCodeLink'] .'" title="Source code"><i class="fas fa-code"></i></a> ';
					?>
				</p>
			</div>
		<?php
	}

	$projects = json_decode(get_option('dried_compositions', '{}'), true);
	$groups = json_decode(get_option('dried_groups', '{}'), true);

	// add uncategorized group
	global $UNCATEGORIZED_SLUG;
	$UNCATEGORIZED_SLUG = 'uncategorized';
	$groups[$UNCATEGORIZED_SLUG] = array(
		"name" => "Other",
		"slug" => $UNCATEGORIZED_SLUG,
		"sort" => "99"
	);

	/**
	 * sorter function (by yearSort)
	 */
	function sorter($a, $b) {
		if ($a['yearSort'] == $b['yearSort']) {
			return 0;
		}
		return ($a['yearSort'] > $b['yearSort']) ? -1 : 1;
	}

	/**
	 * find a group by its slug
	 * the groups are indexed by slugs, but I don't want to 
	 * use a user-provided value as an index
	 * 
	 * returns false if no group found
	 */
	function &get_group_by_slug($slug, &$groups) {
		foreach($groups as &$group)
			if ($group['slug'] == $slug)
				return $group;

		global $UNCATEGORIZED_SLUG;
		return $groups[$UNCATEGORIZED_SLUG];
	}

	// ensure each group has a projects container
	foreach($groups as &$group) {
		$group["projects"] = array();
	}
	unset($group);

	// sort groups order (keeping key association)
	uasort($groups, function($a, $b) {
		if ($a['sort'] == $b['sort'])
			return 0;
		return $a['sort'] < $b['sort'] ? -1 : 1;
	});

	// sort projects into groups
	foreach($projects as $c) {
		if ($c['hide']) // don't do anything with hidden projects
			continue;

		$group = &get_group_by_slug($c['group'], $groups);
		if (!$group)
			$group = $groups[$UNCATEGORIZED_SLUG]; // this slug is safe, we just set it

		array_push($group['projects'], $c);
	}

	// render groups and projects
	foreach($groups as &$group) {
		if (count($group['projects']) < 1) // don't render empty groups
			continue;

		usort($group['projects'], "sorter");

		echo "<h2>{$group['name']}</h2>";
		foreach($group['projects'] as $project)
			render_one_project($project);
	}
	unset($group);

	echo '</article>';

	get_footer(); 
?>