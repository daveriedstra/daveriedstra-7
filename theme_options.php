<?php

global $dried_ajax_nonce_string;
$dried_ajax_nonce_string = '576d8480f05357f405918c9b5bac8794';


/**
*		Adds the composition theme menu page
*
*/
function register_compositions_page() {
	add_menu_page('Compositions', 'Compositions', 'edit_others_posts', 'slug', 'render_compositions_page', 'dashicons-playlist-audio', 6);
	wp_register_script( 'angular', '//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.min.js', array('jquery'), null, true);
	wp_register_script( 'angular-dragdrop', get_stylesheet_directory_uri() . '/js/vendor/draganddrop.min.js', array('angular'), null, true);
	wp_register_script( 'dried-utils', get_stylesheet_directory_uri() . '/js/dried-utils.js', array('angular'), null, true);
	wp_register_script( 'compositions-page', get_stylesheet_directory_uri() . '/js/admin-compositions-page.js', array('angular', 'angular-dragdrop', 'dried-utils'), null, true);

	global $dried_ajax_nonce_string;

	// config object sent to js
	$dried = array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'ajax_nonce' => wp_create_nonce( $dried_ajax_nonce_string ),
		'compositions' => array(),
		'groups' => array(),
		'errors' => array()
	);

	$dried = add_option_value('compositions', $dried);
	$dried = add_option_value('groups', $dried);
	wp_localize_script( 'dried-utils', 'dried', $dried );
}
add_action('admin_menu', 'register_compositions_page');

/**
 * shorthand to get value of a get_option result and add to a config object
 */
function add_option_value($opt_name, $cfg_ob) {
	$val = json_decode(get_option("dried_$opt_name", '{}'));
	if ( !is_null($val) ) { // only send compositions if valid JSON or not set
		$cfg_ob[$opt_name] = $val;
	} else {
		$cfg_ob['errors'][] = array($opt_name, 'malformatted');
	}
	return $cfg_ob;
}

/**
*		Renders the menu which allows manipulation of compositions
*
*/
function render_compositions_page() {
	wp_enqueue_media();
	wp_enqueue_style( 'compositions-page-styles', get_stylesheet_directory_uri() . '/css/admin-compositions-page.css');
	wp_enqueue_script('compositions-page');
?>

	<div id="dried-compositions-page" ng-app="dried" ng-controller="CompositionsPage">
		<div ng-bind="ajaxStatus" id="ajax-status"></div>
		<h2>Groups</h2>
		<div>
			<p class="input-group">
				<label for="group-name">Name</label>
				<input type="text" name="group-name" ng-model="stagedGroup.name" />
			</p>

			<p class="input-group">
				<label for="group-sort">Sort</label>
				<input type="number" name="group-sort" ng-model="stagedGroup.sort" />
			</p>
			<input type="button" value="Add" ng-click="addGroup()" class="button" />
		</div>
		<div class="composition-list">
			<h3>Current entries</h3>
			<div ng-repeat="(key, g) in groups | orderObjectBy : 'sort'">
				<h4 class="collapse-toggle" ng-click="stageGroup(g)">{{g.name}} ({{g.sort}}) <small><a href="#" ng-click="$event.preventDefault(); deleteGroup(g);">delete</a></small></h4>
			</div>
		</div>

		<h2>Compositions</h2>
		<div class="composition-input">
			<p>Add some compositions here (<small><a href="#" ng-click="resetIndices(); $event.preventDefault();">reset indices</a></small>)</p>

			<p class="input-group">
				<label for="name">Name</label>
				<input type="text" name="name" ng-model="stagedWork.name" />
			</p>

			<p class="input-group">
				<label for="formattedName">Formatted Name</label>
				<input type="text" name="formattedName" ng-model="stagedWork.formattedName" />
			</p>

			<p class="input-group input-group-checkbox">
				<label><input type="checkbox" name="switchyName" ng-model="stagedWork.switchyName" /> Switchy?</label>
			</p>

			<p class="input-group input-group-checkbox">
				<label><input type="checkbox" name="reuse" ng-model="stagedWork.reuse" /> Reuse characters?</label>
			</p>

			<p class="input-group input-group-checkbox">
				<label><input type="checkbox" name="delimiters" ng-model="stagedWork.delimiters" /> Delimiters?</label>
			</p>


			<p class="input-group">
				<label for="year">Year</label>
				<input type="text" name="year" ng-model="stagedWork.year" />
			</p>

			<p class="input-group">
				<label for="year-sort">Year <small>(sort)</small></label>
				<input type="date" name="year-sort" ng-model="stagedWork.yearSort" />
			</p>

			<p class="input-group">
				<label for="instrumentation">Instrumentation</label>
				<input type="text" name="instrumentation" ng-model="stagedWork.instrumentation" />
			</p>

			<p class="input-group">
				<label for="group">Group</label>
				<select
					ng-model="stagedWork.group"
					ng-options="g.slug as g.name for g in groups | orderObjectBy : 'sort'"
				></select>
			</p>

			<p class="input-group">
				<label for="desc">Description</label>
				<input type="text" name="desc" ng-model="stagedWork.desc" />
			</p>

			<p class="input-group">
				<label for="duration">Duration</label>
				<input type="text" name="duration" ng-model="stagedWork.duration" />
			</p>

			<p class="input-group">
				<label for="infoLink">Info URL</label>
				<input type="text" name="infoLink" ng-model="stagedWork.infoLink" />
			</p>

			<p class="input-group">
				<label for="audioLink">Audio URL</label>
				<input type="text" name="audioLink" ng-model="stagedWork.audioLink" />
			</p>

			<p class="input-group">
				<label for="videoLink">Video URL</label>
				<input type="text" name="videoLink" ng-model="stagedWork.videoLink" />
			</p>

			<p class="input-group">
				<label for="score">Score</label>
				<input type="text" name="score" ng-model="stagedWork.score" ng-click="showMediaPicker(stagedWork, 'score')" />
			</p>

			<p class="input-group">
				<label for="sourceCodeLink">Source code URL</label>
				<input type="text" name="sourceCodeLink" ng-model="stagedWork.sourceCodeLink" />
			</p>

			<input type="button" value="Add" ng-click="addWork()" class="button" />
		</div>

		<div class="composition-list">
			<h3>Current entries</h3>
			<div
				ng-repeat="(key, c) in compositions | orderObjectBy : 'yearSort' : true"
				ng-class="{inactive: c.hide}"
			>
				<h4
					ng-init="c.collapsed = true"
					ng-click="c.collapsed = !c.collapsed"
					class="collapse-toggle"
				>{{c.name}} <small><a href="#" ng-click="$event.preventDefault(); deleteWork(c.name);">delete</a></small>
				</h4>
				<div
					class="collapsible collapsed"
					ng-class="{collapsed: c.collapsed}"
				>
					<p class="input-group">
						<label for="name">Name</label>
						<input ng-blur="saveCompositions()" type="text" name="name" ng-model="c.name" />
					</p>

					<p class="input-group">
						<label for="formattedName">Formatted Name</label>
						<input ng-blur="saveCompositions()" type="text" name="formattedName" ng-model="c.formattedName" />
					</p>

					<p class="input-group input-group-checkbox">
						<label><input ng-blur="saveCompositions()" type="checkbox" name="switchyName" ng-model="c.switchyName" /> Switchy?</label>
					</p>

					<p class="input-group input-group-checkbox">
						<label><input ng-blur="saveCompositions()" type="checkbox" name="reuse" ng-model="c.reuse" /> Reuse characters?</label>
					</p>

					<p class="input-group input-group-checkbox">
						<label><input ng-blur="saveCompositions()" type="checkbox" name="delimiters" ng-model="c.delimiters" /> Delimiters?</label>
					</p>

					<p class="input-group input-group-checkbox">
						<label><input ng-blur="saveCompositions()" type="checkbox" name="hide" ng-model="c.hide" /> Hide?</label>
					</p>

					<p class="input-group">
						<label for="year">Year</label>
						<input ng-blur="saveCompositions()" type="text" name="year" ng-model="c.year" />
					</p>

					<p class="input-group">
						<label for="year-sort">Year <small>(sort)</small></label>
						<input ng-blur="saveCompositions()" type="date" name="year-sort" ng-model="c.yearSort" />
					</p>

					<p class="input-group">
						<label for="group">Group</label>
						<select
							ng-blur="saveCompositions()"
							ng-model="c.group"
							ng-options="g.slug as g.name for g in groups | orderObjectBy : 'sort'"
						></select>
					</p>

					<p class="input-group">
						<label for="instrumentation">Instrumentation</label>
						<input ng-blur="saveCompositions()" type="text" name="instrumentation" ng-model="c.instrumentation" />
					</p>

					<p class="input-group">
						<label for="desc">Description</label>
						<input ng-blur="saveCompositions()" type="text" name="desc" ng-model="c.desc" />
					</p>

					<p class="input-group">
						<label for="duration">Duration</label>
						<input ng-blur="saveCompositions()" type="text" name="duration" ng-model="c.duration" />
					</p>

					<p class="input-group">
						<label for="infoLink">Info URL</label>
						<input ng-blur="saveCompositions()" type="text" name="infoLink" ng-model="c.infoLink" />
					</p>

					<p class="input-group">
						<label for="audioLink">Audio URL</label>
						<input ng-blur="saveCompositions()" type="text" name="audioLink" ng-model="c.audioLink" />
					</p>

					<p class="input-group">
						<label for="videoLink">Video URL</label>
						<input ng-blur="saveCompositions()" type="text" name="videoLink" ng-model="c.videoLink" />
					</p>

					<p class="input-group">
						<label for="score">Score</label>
						<input ng-blur="saveCompositions()" type="text" name="score" ng-model="c.score" ng-click="showMediaPicker(compositions[c.name], 'score', true)" />
					</p>

					<p class="input-group">
						<label for="sourceCodeLink">Source code URL</label>
						<input ng-blur="saveCompositions()" type="text" name="sourceCodeLink" ng-model="c.sourceCodeLink" />
					</p>
				</div>
			</div>
		</div>
	</div>

<?php
}

function dried_can_save($opt_name) {
	global $dried_ajax_nonce_string;
	if (!check_ajax_referer( $dried_ajax_nonce_string, 'ajax_nonce', false )) {
		send_fail('bad_nonce', 'Nonce is incorrect (it may have timed out)');
		return false;
	}

	if (!is_user_logged_in() || !current_user_can('edit_posts')) {
		send_fail('nopriv', 'User is not logged in or does not have required permissions.');
		return false;
	}

	if ( !isset($_POST[$opt_name]) || is_null(json_decode(stripslashes($_POST[$opt_name]))) ) {
		send_fail('malformatted', "$opt_name data is malformatted");
		return false;
	}

	return true;
}

/*
*		AJAX endpoint to save compositions
*
*/
add_action('wp_ajax_save_compositions', 'save_compositions');
function save_compositions () {
	if (dried_can_save('works'))
		update_option('dried_compositions', stripslashes($_POST['works'])); // save the JSON

	send_success();
}

/*
*		AJAX endpoint to save composition groups
*
*/
add_action('wp_ajax_save_groups', 'save_groups');
function save_groups () {
	if (dried_can_save('groups'))
		update_option('dried_groups', stripslashes($_POST['groups'])); // save the JSON

	send_success();
}
