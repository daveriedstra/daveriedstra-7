<?php
    // meta for digital_project posts
    require_once(get_template_directory() . '/DMeta/class-DMeta.php');
    $DM = DMeta::get_instance();
    // registers with DMeta
    $DM->register_meta(
        array(
            'name' => 'order',
            'label' => 'Order',
            'description' => 'Order in which this project will appear (lower = earlier)',
            'input_type' => 'number',
			'step' => 1,
            'data_type' => DMeta::DATA_TYPES['INT']
        ),
        'meta_queue' // name of the meta queue
    );

    // register with WP
    function add_digital_project_meta ($post) {
        add_meta_box(
            'order',
            __( 'Order' ),
            'render_digital_project_meta',
            'digital_project'
        );
    }
    add_action('add_meta_boxes_digital_project', 'add_digital_project_meta');

    // add a nonce and render meta inputs
    function render_digital_project_meta($post) {
        wp_nonce_field('render_meta_box', 'section_nonce');

        $DM = DMeta::get_instance();
        $DM->render_queue($post->ID, 'meta_queue');
    }

    // check nonce and save meta
    function save_post_meta ($post_id) {
        if (!can_save_meta($post_id, 'render_meta_box', 'section_nonce'))
            return $post_id;

        $DM = DMeta::get_instance();
        $DM->save_queue($post_id, 'meta_queue');
    }
    add_action('save_post', 'save_post_meta');

    // can the user save the meta?
    function can_save_meta($post_id, $nonce_action, $nonce_name) {
        return isset($_POST[$nonce_name]) && wp_verify_nonce($_POST[$nonce_name], $nonce_action) && current_user_can('edit_post', $post_id);
    }

    // ensure meta is added for rest
    function add_digital_project_rest_meta() {
        register_rest_field('digital_project', 'post_meta', [
            'get_callback' => function($post) {
                return get_post_meta($post['id']);
            }
        ]);
        register_rest_field('digital_project', 'order', [
            'get_callback' => function($post) {
                return get_post_meta($post['id'], 'order', true);
            }
        ]);
    }
    add_filter('rest_api_init', 'add_digital_project_rest_meta');
?>
