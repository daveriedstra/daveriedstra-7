<?php
    // meta for digital_project posts
    require_once(get_template_directory() . '/DMeta/class-DMeta.php');
    $DM = DMeta::get_instance();
    // registers with DMeta
    $DM->register_meta(
        array(
            'name' => 'categories',
            'label' => 'Categories',
            'description' => 'Categories to show on this page (passed directly to WP_QUERY)',
            'input_type' => 'text',
            'data_type' => DMeta::DATA_TYPES['STRING']
        ),
        'articles_meta_queue' // name of the meta queue
    );

    // register with WP
    function add_articles_meta ($post) {
		if (get_page_template_slug($post->ID) === 'articles_template.php') {
			add_meta_box(
				'categories',
				__( 'Categories' ),
				'render_articles_meta',
				'page',
				'normal'
			);
		}
    }
    add_action('add_meta_boxes_page', 'add_articles_meta');

    // add a nonce and render meta inputs
    function render_articles_meta($post) {
        wp_nonce_field('render_articles_box', 'section_nonce');

        $DM = DMeta::get_instance();
        $DM->render_queue($post->ID, 'articles_meta_queue');
    }

    // check nonce and save meta
    function save_articles_meta ($post_id) {
        if (!can_save_articles_meta($post_id, 'render_articles_box', 'section_nonce'))
            return $post_id;

        $DM = DMeta::get_instance();
        $DM->save_queue($post_id, 'articles_meta_queue');
    }
    add_action('save_post', 'save_articles_meta');

    // can the user save the meta?
    function can_save_articles_meta($post_id, $nonce_action, $nonce_name) {
		$is_articles = get_page_template_slug($post_id) === 'articles_template.php';
        return isset($_POST[$nonce_name]) && wp_verify_nonce($_POST[$nonce_name], $nonce_action) && current_user_can('edit_post', $post_id) && $is_articles;
    }

    // ensure meta is added for rest
    function add_articles_rest_meta() {
        register_rest_field('articles', 'post_meta', [
            'get_callback' => function($post) {
                return get_post_meta($post['id']);
            }
        ]);
        register_rest_field('articles', 'categories', [
            'get_callback' => function($post) {
                return get_post_meta($post['id'], 'categories', true);
            }
        ]);
    }
    add_filter('rest_api_init', 'add_articles_rest_meta');
?>
