<?php
	/*
	*		USE THIS FILE TO RESET COMPOSITIONS TO DATA FROM NOV 2018
	*		IN CASE OF EMERGENCY
	*/

	// save the shit
	$projects = array(
		"wifi" => array(
		  "name" => "wifi",
		  "formattedName" => "802.11",
		  "year" => "2016 – 2018",
		  "instrumentation" => "Raspberry Pis, wireless network adapters, transducers, Raspbian, Python, Pure Data",
		  "desc" => "conflations of affordances, aesthetic, and environments",
		  "duration" => "installation",
		  "score" => "",
		  "videoLink" => "https://www.youtube.com/watch?v=-iLXWpIyeNs"
		),
		"4voices" => array(
		  "name" => "4voices",
		  "formattedName" => "—–-−-–--−",
		  "switchyName" => true,
		  "reuse" => true,
		  "year" => "2015 (rev. 2016)",
		  "instrumentation" => "four musicians",
		  "desc" => "ʃʃʃ",
		  "duration" => "0 – many min.",
		  "videoLink" => "https://youtu.be/liMZOhVOJtI"
		),
		"3voices" => array(
		  "name" => "3voices",
		  "formattedName" => "]               　[",
		  "switchyName" => true,
		  "reuse" => true,
		  "delimiters" => true,
		  "year" => "2016",
		  "instrumentation" => "three singers",
		  "desc" => "Not un-singing",
		  "duration" => "0 – many min.",
		  "audioLink" => "https://soundcloud.com/dried/zzjflwk3jxrv"
		),
		"2basses" => array(
		  "name" => "2basses",
		  "formattedName" => "‖∥∣ǀǁ∣∥∥‖∥‖∥‖∥∣‖∥‖∥∥∥∥∥",
		  "switchyName" => true,
		  "reuse" => true,
		  "videoLink" => "https://www.youtube.com/watch?v=VaEh4SzUr5I",
		  "year" => "2016",
		  "instrumentation" => "2 electric basses",
		  "desc" => "F",
		  "duration" => "0 – many min."
		),
		"trombone" => array(
		  "name" => "trombone",
		  "formattedName" => "Trombone Solo",
		  "videoLink" => "https://www.youtube.com/watch?v=r1SpSuetiW8",
		  "year" => "2015",
		  "instrumentation" => "trombone solo",
		  "desc" => "A trombone solo",
		  "duration" => "13 min."
		),
		"montreal" => array(
		  "name" => "montreal",
		  "formattedName" => "0Ooº°oO",
		  "switchyName" => true,
		  "reuse" => true,
		  "audioLink" => "https://soundcloud.com/dried/oo0oo",
		  "year" => "2015",
		  "instrumentation" => "two flutes, French horn, acoustic guitar",
		  "desc" => "Qualitative shufflings",
		  "duration" => "0 – many min."
		),
		"varpiece" => array(
		  "name" => "varpiece",
		  "formattedName" => "var piece;",
		  "year" => "2015",
		  "instrumentation" => "solo for Chrome with live vocalist",
		  "desc" => "An epistemological feedback loop",
		  "duration" => "0 – many min."
		),
		"breach" => array(
		  "name" => "breach",
		  "formattedName" => "Breach",
		  "audioLink" => "https://soundcloud.com/dried/breach",
		  "year" => "2015",
		  "instrumentation" => "any two bowed, like-timbred, four-or-more-stringed instruments",
		  "desc" => "Molecules generating static",
		  "duration" => "0 – many min."
		),
		"shapes" => array(
		  "name" => "shapes",
		  "formattedName" => "●❙■▲▬",
		  "switchyName" => true,
		  "year" => "2014/15",
		  "instrumentation" => "flute, trumpet, bass clarinet, violin, viola, electric guitar",
		  "desc" => "Just-beginnings-to-stir",
		  "duration" => "0 – many min.",
		  "videoLink" => "https://www.youtube.com/watch?v=t9Ufe2EqVrY"
		),
		"prairietrails" => array(
		  "name" => "prairietrails",
		  "formattedName" => "Prairie Trails (43%)",
		  "audioLink" => "https://soundcloud.com/dried/prairie-trails-43",
		  "year" => "2014",
		  "instrumentation" => "bass clarinet solo",
		  "desc" => "An a/effecting of various bodies",
		  "duration" => "20 min."
		),
		"partygames" => array(
		  "name" => "partygames",
		  "hide" => true,
		  "formattedName" => "Party Games",
		  "year" => "2014",
		  "instrumentation" => "open instrumentation",
		  "desc" => "An experiment with threshold-level dynamics and spatialization.",
		  "duration" => "8 min."
		),
		"refactoring" => array(
		  "name" => "refactoring",
		  "formattedName" => "Refactoring",
		  "audioLink" => "https://soundcloud.com/dried/refactoring",
		  "year" => "2014",
		  "instrumentation" => "flute solo",
		  "desc" => "Two distinct blocks meet",
		  "duration" => "7 min."
		),
		"devenir" => array(
		  "name" => "devenir",
		  "hide" => true,
		  "formattedName" => "Devenir",
		  "year" => "2014",
		  "instrumentation" => "piano four-hands",
		  "desc" => "Two wedding gifts, explorations of the various aspects of the instrument and its performance.",
		  "duration" => "5 min."
		),
		"mnemonic" => array(
		  "name" => "mnemonic",
		  "formattedName" => "Mnemonic",
		  "year" => "2014",
		  "audioLink" => "https://soundcloud.com/dried/mnemonic",
		  "linkText" => "listen",
		  "instrumentation" => "string quartet",
		  "desc" => "An experiment in memory composed with and performed by Quatuor Bozzini",
		  "duration" => "6 min."
		),
		"alongyourtrails" => array(
		  "name" => "alongyourtrails",
		  "formattedName" => "Along your trails",
		  "year" => "2014",
		  "instrumentation" => "string quartet",
		  "desc" => "A simple moment",
		  "duration" => "3 min."
		),
		"approximate" => array(
		  "name" => "approximate",
		  "formattedName" => "Approximate",
		  "year" => "2014",
		  "audioLink" => "https://soundcloud.com/dried/approximate",
		  "linkText" => "listen",
		  "instrumentation" => "electric guitar, piano, and string trio",
		  "desc" => "Cubist acousmatics",
		  "duration" => "7 min."
		),
		"bottomfeeder" => array(
		  "name" => "bottomfeeder",
		  "formattedName" => "Bottom Feeder",
		  "year" => "2013/14",
		  "audioLink" => "https://soundcloud.com/dried/bottom-feeder-version-2",
		  "linkText" => "listen",
		  "desc" => "Comparing communication paradigms",
		  "image" => "img/previews/bottomfeeder.jpg",
		  "duration" => "7 min.",
		  "instrumentation" => "soprano voice, alto saxophone, tenor saxophone"
		),
		"listenerspeaker" => array(
		  "name" => "listenerspeaker",
		  "formattedName" => "Listener, Speaker",
		  "year" => "2013",
		  "videoLink" => "https://www.youtube.com/watch?v=3yE-ZPqEcH4",
		  "desc" => "",
		  "instrumentation" => "puredata and web technologies",
		  "duration" => "installation"
		),
		"mysticalhealer" => array(
		  "name" => "mysticalhealer",
		  "formattedName" => "so i'm not some kind of mystical healer",
		  "year" => "2012/14",
		  "videoLink" => "http://www.youtube.com/watch?v=X9eyzO5R9N0",
		  "instrumentation" => "piano solo",
		  "desc" => "Clamour",
		  "duration" => "2.5 min."
		),
		"lookup" => array(
		  "name" => "lookup",
		  "hide" => true,
		  "formattedName" => "look up from your phone to the crumbling world around you",
		  "year" => "2012",
		  "instrumentation" => "oboe, violin, and accordion",
		  "duration" => "5 min.",
		  "type" => "music"
		),
		"hazing" => array(
		  "name" => "hazing",
		  "type" => "music",
		  "formattedName" => "Hazing",
		  "year" => "2012",
		  "duration" => "10.5 min.",
		  "instrumentation" => "tenor voice, clarinet, piano, SATB chamber choir"
		),
		"pare" => array(
		  "name" => "pare",
		  "formattedName" => "Pare",
		  "year" => "2012",
		  "audioLink" => "https://soundcloud.com/dried/pare",
		  "linkText" => "listen",
		  "instrumentation" => "oboe solo",
		  "duration" => "5 min.",
		  "desc" => "Straight lines"
		),
		"hairsbreadth" => array(
		  "name" => "hairsbreadth",
		  "formattedName" => "Hair's Breadth",
		  "year" => "2012",
		  "audioLink" => "https://soundcloud.com/dried/hairs-breadth",
		  "linkText" => "listen",
		  "desc" => "After Henry Threadgill",
		  "image" => "img/previews/hairsbreadth.png",
		  "extraLink" => "charts/dave_riedstra_hairs_breadth.pdf",
		  "extraLinkText" => "chart",
		  "instrumentation" => "jazz quartet",
		  "duration" => "open"
		),
		"duetforviolinandpiano" => array(
		  "name" => "duetforviolinandpiano",
		  "formattedName" => "Duet for violin and piano",
		  "year" => "2011",
		  "audioLink" => "https://soundcloud.com/dried/duet-for-violin-and-piano-1",
		  "linkText" => "listen",
		  "desc" => "A dismantling",
		  "duration" => "3 min.",
		  "instrumentation" => "violin, piano"
		),
		"bartokandthegeranium" => array(
		  "name" => "bartokandthegeranium",
		  "formattedName" => "Bartók and the Geranium",
		  "year" => "2011",
		  "audioLink" => "https://soundcloud.com/dried/bartok-and-the-geranium",
		  "linkText" => "listen",
		  "desc" => "A setting of the poem by Dorothy Livesay",
		  "duration" => "4 min.",
		  "instrumentation" => "SSATTB chamber choir"
		),
		"floatinginapeacefulsea" => array(
		  "name" => "floatinginapeacefulsea",
		  "formattedName" => "Floating in a Peaceful Sea",
		  "year" => "2011",
		  "audioLink" => "https://soundcloud.com/dried/floating-in-a-peaceful-sea",
		  "linkText" => "listen",
		  "desc" => "A meandering",
		  "extraLink" => "charts/dave_riedstra_floating_in_a_peaceful_sea.pdf",
		  "extraLinkText" => "chart",
		  "instrumentation" => "jazz combo",
		  "duration" => "open"
		),
		"apprehension" => array(
		  "name" => "apprehension",
		  "hide" => true,
		  "formattedName" => "Apprehension",
		  "year" => "2010",
		  "instrumentation" => "clarinet, alto saxophone, tenor saxophone, piano, contrabass, video accompaniment",
		  "duration" => "4 min.",
		  "type" => "music"
		),
		"occupation" => array(
		  "name" => "occupation",
		  "formattedName" => "Occupation",
		  "year" => "2010",
		  "instrumentation" => "clarinet, alto saxophone, tenor saxophone, piano, and contrabass",
		  "desc" => "A micropolyphonic funnel",
		  "duration" => "7 min."
		),
		"veils" => array(
		  "name" => "veils",
		  "formattedName" => "Veils",
		  "year" => "2016",
		  "instrumentation" => "violin solo",
		  "desc" => "\"Veils pile up and veil only other veils\"",
		  "duration" => "15 min.",
		  "hide" => false,
		  "audioLink" => "https://soundcloud.com/dried/veils"
		),
		"kimandjulio" => array(
		  "name" => "kimandnatasha",
		  "formattedName" => "(untitled)",
		  "year" => "2017",
		  "instrumentation" => "piano and violin",
		  "desc" => "(in progress)",
		  "duration" => "0 – many min.",
		  "hide" => true
		),
		"treatment" => array(
		  "name" => "treatment",
		  "formattedName" => "Treatment",
		  "year" => "2017",
		  "instrumentation" => "puredata, raspberry pi, speakers",
		  "duration" => "installation"
		),
		"atlantica" => array(
		  "name" => "atlantica",
		  "formattedName" => "· ˙ · • .  ִ ֹ ּ ׁ ׂ ׄ ׅ ٜ ۬ ਂ ਼ ં ઼ ᆞ ᐧ ᛫ ᠂ ․ ‧ ∙ ⠁ ⠂ ⠄ ⠈ ・",
		  "switchyName" => true,
		  "year" => "2017",
		  "desc" => "(in progress)",
		  "duration" => "0 – many",
		  "instrumentation" => "ensemble",
		  "hide" => true
		),
		"fulcra" => array(
		  "name" => "fulcra",
		  "formattedName" => "Fulcra",
		  "year" => "2017",
		  "instrumentation" => "clarinet, violin, piano",
		  "duration" => "recommended: 40 – 60 sec.",
		  "desc" => "\"paths that traverse apparent impasses, proceeding by aporia\"",
		  "audioLink" => "https://soundcloud.com/dried/fulcra"
		),
		"aosp-sensors" => array(
		  "name" => "aosp-sensors",
		  "formattedName" => "AOSP Sensors",
		  "year" => "2018",
		  "instrumentation" => "Nexus S (crespo), Android SDK 18, Pure Data",
		  "videoLink" => "https://www.youtube.com/watch?v=pX8uRk_z6No",
		  "desc" => "",
		  "duration" => "installation"
		),
		"burl" => array(
		  "name" => "burl",
		  "formattedName" => "Burl",
		  "year" => "2018",
		  "instrumentation" => "contrabass solo",
		  "duration" => "30 min."
		)
	  );

	update_option('dried_compositions', json_encode($projects));

	// make voice trio title
	// $voice_trio = 
	// $voice_trio_pool = array(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','　');
	// foreach ($projects as &$project) {
	// 	if ($project['name'] == '3voices') {
	// 		shuffle($project['titlePool']);
	// 		$project['formattedName'] = ']' . implode($project['titlePool']) . '[';
	// 		break;
	// 	}
	// }
	// unset($project);
?>