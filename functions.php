<?php

	// Add RSS links to <head> section
	add_theme_support('automatic_feed_links');

	// Clean up the <head>
	function removeHeadLinks() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
	}
	add_action('init', 'removeHeadLinks');
	remove_action('wp_head', 'wp_generator');


	// declare main menu
	register_nav_menu('main_nav', 'Main navigation for site');

	// enqueue scripts
	function dried_scripts() {
		if (is_page_template('digital_template.php'))
			wp_enqueue_script(
				'digital',
				get_template_directory_uri() . '/js/digital.js',
				[],
				false,
				true // in footer
			);
	}
	add_action('wp_enqueue_scripts', 'dried_scripts');

	/*
	*		checks that a property is set and is true
	*/
	function prop_is_true($o, $p) {
		return (isset($o[$p]) && $o[$p] === true);
	}

	function make_img_sizes() {
		add_image_size( 'bg-750', 750 );
		add_image_size( 'bg-1440', 1440 );
		add_image_size( 'bg-1920', 1920 );
		add_image_size( 'bg-2880', 2880 );
	}
	add_action( 'after_setup_theme', 'make_img_sizes' );

	/*
	*		Makes a (composition) project name, doing switching where necessary
	*/
	function make_proj_name($proj) {
		if (prop_is_true($proj, 'switchyName')) {
			$delims = array('', '');

			// set and remove delimiters if necessary
			if (prop_is_true($proj, 'delimiters')) {
				// get
				$delims[0] = $proj['formattedName'][0];
				$delims[1] = substr($proj['formattedName'], -1);

				// trim
				$proj['formattedName'] = substr($proj['formattedName'], 1, strlen($proj['formattedName']) - 2);
			}

			// split into array of chars
			// have to use this instead of str_split for multibyte chars
			// see discussion here: http://php.net/manual/en/function.mb-split.php
			$proj['formattedName'] = preg_split('/(?<!^)(?!$)/u', $proj['formattedName']);

			// multiply some chars reusing chars
			if (prop_is_true($proj, 'reuse')) {
				$add = array();
				foreach ($proj['formattedName'] as $key => $char) {
					while (random_int(0, 1) < 0.3) {
						$add[] = $char;
					}
				}
				$proj['formattedName'] = array_merge($proj['formattedName'], $add);
			}

			// switch without reusing chars
			shuffle($proj['formattedName']);
			$proj['formattedName'] = implode('', $proj['formattedName']);

			// return with delimiters
			return $delims[0] . $proj['formattedName'] . $delims[1];
		} else {
			return $proj['formattedName'];
		}
	}

	/*
	*		Sends a fail message to an AJAX request
	*
	*/
	function send_fail($code = '', $message = '') {
		header( "Content-Type: application/json" );
		echo json_encode(
			array(
				'success' => false,
				'code' => $code,
				'message' => $message
			)
		);
		wp_die();
	}

	/*
	*		Sends a success response to an AJAX request
	*
	*/
	function send_success($data = array()) {
		header( "Content-Type: application/json" );
		echo json_encode(array(
				'success' => true,
				'data' => $data
			)
		);
		wp_die();
	}


	/*
	*		Creates "Technologies" taxonomy for Web projects
	*
	*/
	add_action('init', 'create_technologies_taxonomy');
	function create_technologies_taxonomy() {
		register_taxonomy('technologies', array('web', 'digital_project'),
			array(
				'labels' => array(
					'name' => 'Technologies',
					'singular_name' => 'Technology'
					),
                'show_in_rest' => true,
				'description' => 'Technologies used in a web project (eg, Angular, Node, Mongo)'
				)
			);
	}

	/*
	*		Creates the Web Project post type
	*
	*/
	add_action( 'init', 'create_web_post_type' );
	function create_web_post_type() {
		register_post_type( 'web',
			array(
				'rewrite' => array('slug' => 'project'),
				'labels' => array(
					'name' => __( 'Web Projects' ),
					'singular_name' => __( 'Web Project' )
					),
				'description' => 'A web portfolio project',
				'exclude_from_search' => false,
				'publicly_queryable' => true,
				'show_in_nav_menus' => false,
                'show_in_rest' => true,
				'show_ui' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-analytics',
				'menu_position' => 7,
				'taxonomies' => array('technologies')
				)
			);
	}

    /**
     *  Creates "Filter" taxonomy for Digital projects
     *
     */
    add_action('init', 'create_filter_taxonomy');
    function create_filter_taxonomy() {
        register_taxonomy('filter', array('digital_project'),
            array(
                'labels' => array(
                    'name' => 'Filters',
                    'singular_name' => 'Filter'
                    ),
                'show_in_rest' => true,
                'description' => 'Filter terms for Digital projects'
                )
            );
    }

	/*
	*		Creates the Digital post type
	*
	*/
	add_action( 'init', 'create_digital_post_type' );
	function create_digital_post_type() {
		register_post_type( 'digital_project',
			array(
				'rewrite' => array('slug' => 'digital_project'),
				'labels' => array(
					'name' => __( 'Digital Projects' ),
					'singular_name' => __( 'Digital Project' )
					),
				'description' => 'A digital project',
				'exclude_from_search' => false,
				'publicly_queryable' => true,
				'show_in_nav_menus' => false,
                'show_in_rest' => true,
				'show_ui' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-analytics',
				'menu_position' => 7,
                'supports' => array(
                    'title',
                    'editor',
                    // 'comments',
                    'revisions',
                    // 'trackbacks',
                    // 'author',
                    // 'excerpt',
                    // 'page-attributes',
                    'thumbnail',
                    // 'custom-fields',
                    'post-formats'
                    ),
				'taxonomies' => array('technologies', 'filter')
				)
			);
        add_theme_support('post-thumbnails', array('digital_project'));
	}
	// register digital_project meta
	require_once(get_template_directory() . '/inc/digital_project_meta.php');
	// register articles template meta
	require_once(get_template_directory() . '/inc/articles_template_meta.php');
	// register theme options page
	require_once(get_template_directory() . '/theme_options.php');

	// ONLY INCLUDE FOR EMERGENCY
	// require_once(get_template_directory() . '/inc/reset_works.php');
?>
