<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		   	  $is_home = is_home() || is_front_page();

		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && !($is_home) && (is_single() || is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if ($is_home) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	
	<link rel="shortcut icon" href="/favicon.ico">
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&amp;subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<!-- head -->
	<header class="site-header">
		<div class="header__left">
			<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
			<a href="<?php echo get_option('home'); ?>/"><img src="http://daveriedstra.com/home/wp-content/uploads/2017/11/header-sm.jpg" class="circle headshot" width="160" /></a><?php // theme option eventually ?>
			<p class="incipit"><?php bloginfo('description'); ?></p>
		</div>
		<div class="header__right">
			<div class="social">
				<a href="mailto:dave@daveriedstra.com"><img height="21" src="<?php echo get_template_directory_uri(); ?>/img/icons-2/email.svg" /></a>
				<a href="https://twitter.com/driedstr"><img height="21" src="<?php echo get_template_directory_uri(); ?>/img/icons-2/twitter.svg" /></a>
				<a href="http://ca.linkedin.com/pub/dave-riedstra/20/b89/a04/"><img height="21" src="<?php echo get_template_directory_uri(); ?>/img/icons-2/linkedin.svg" /></a>
				<a href="https://soundcloud.com/dried"><img height="21" src="<?php echo get_template_directory_uri(); ?>/img/icons-2/soundcloud.svg" /></a>
				<a href="https://www.youtube.com/channel/UCqiKSHOjuaezsQoRZNotgeQ"><img height="21" src="<?php echo get_template_directory_uri(); ?>/img/icons-2/youtube.svg" /></a>
			</div>
			<nav class="main-nav clearfix">
				<?php wp_nav_menu(array('menu' => 'main_nav', 'container' => '')); ?>
			</nav>
		</div>
	</header>
