<?php 
	/**
	 *		Template Name: Digital
	 *
	 *		AJAX loop to show Digital posts with fancy bg action
	 */
	get_header();
?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/digital.css" />

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="wrap category-intro post" id="post-<?php the_ID(); ?>">
			<h1 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
			<?php the_content(); ?>

			<h1>Projects</h1>
		</div>
	<?php endwhile; endif; ?>

	<ul class="filter-list wrap">Filters: 
		<?php
			$filters = get_terms( array(
				'taxonomy' => 'filter',
				'hide_empty' => true
			));
			foreach ($filters as $filter):
		?>
			<li class="filter filter__list-item">
				<input
					type="checkbox"
					id="<?php echo $filter->term_id; ?>"
					data-filter-slug="<?php echo $filter->slug; ?>"
					class="filter__input"
				/>
				<label
					for="<?php echo $filter->term_id; ?>"
					class="filter__label"
				>
					<?php echo $filter->name; ?>
				</label>
			</li>	
		<?php endforeach; ?>
	</ul>

	<div class="wrap" id="projects-container">
		<?php
			global $wp_query;
			$projects_query = array(
				'post_type' => 'digital_project',
				'post_status' => 'publish',
				'order' => 'ASC',
				'orderby' => 'meta_value_num',
				'meta_key' => 'order'
			);
			query_posts($projects_query);
			if ( have_posts() ) : while ( have_posts() ) : the_post();
		?>
        <article 
			<?php post_class('wrap digital-project clearfix'); ?> 
			id="post-<?php the_ID(); ?>"
		>
            <header>
                <h1 class="post-title highlight"><?php the_title(); ?></h1>
                <aside class="technologies">
                    <p><small class="highlight"><em>
						<?php 
							$technologies = get_the_terms($post->ID, 'technologies'); 
							$technologies = array_map(function($t) { return $t->name; }, $technologies);
							echo implode(", ", $technologies);
						?>
					</em></small></p>
                </aside>
			</header>
            <div class="entry">
				<?php the_post_thumbnail('large'); ?>
				<?php the_content(); ?>
            </div>
        </article>
		<?php endwhile; endif; ?>
	</div>

<?php get_footer(); ?>
