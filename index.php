<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article <?php post_class('wrap') ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
				<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
			</header>
			<div class="entry">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; ?>

	<?php else: ?>
		<div class="wrap">
			<h2>Not Found</h2>
		</div>
	<?php endif; ?>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

<?php get_footer(); ?>
