<?php
	/**
	*		Template Name: Articles
	*
	*		Supports a short blurb and a list of published articles.
	*/
	get_header();
?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="wrap category-intro post" id="post-<?php the_ID(); ?>">
			<h1 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
			<div class="entry">
				<?php the_content(); ?>
			</div>
		</div>

	<?php
		// get categories of articles to show
		$categories = get_post_meta(get_the_ID(), 'categories', true);
	?>
	<?php endwhile; endif; ?>

<?php
		global $wp_query;
		$articles_query = array(
			'category_name' => $categories,
			'post_status' => array('publish', 'private'),
			'perm' => 'readable'
		);
		if (isset($wp_query->query["paged"])) {
			$articles_query['paged'] = $wp_query->query["paged"];
		}
		query_posts($articles_query);
	?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article <?php post_class('wrap') ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
				<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
			</header>
			<div class="entry">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; else: endif; ?>

	<?php
		if (get_next_posts_link() || get_previous_posts_link())
			include (TEMPLATEPATH . '/inc/nav.php' );
	?>

<?php get_footer(); ?>
