<?php get_header(); ?>

	<?php if (have_posts()) : ?>
		<div class="wrap category-intro post">
			<h1>Search Results</h1>
		</div>

		<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

		<?php while (have_posts()) : the_post(); ?>

			<article <?php post_class('wrap') ?> id="post-<?php the_ID(); ?>">
				<header>
					<h1 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
					<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
				</header>
				<div class="entry">
					<?php the_content(); ?>
				</div>
			</article>

		<?php endwhile; ?>

		<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>
		<div class="wrap category-intro post">
			<h1>No posts found.</h1>
		</div>
	<?php endif; ?>

<?php get_footer(); ?>