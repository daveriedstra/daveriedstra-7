"use strict";
(function() {
    function onFilterChange() {
        const filters = Array.from(document.getElementsByClassName("filter__input"))
            .map(e => {
                return {slug: e.dataset.filterSlug, checked: e.checked}
            });
        const isEmpty = filters.reduce((acc, cur) => acc && !cur.checked, true);

        Array.from(document.getElementsByClassName("digital-project"))
            // first query DOM
            .map(el => {
                const terms = Array.from(el.classList)
                    .filter(c => c.startsWith('filter'))
                    .map(t => t.replace(/([A-Z])/g, m => `-${m.toLowerCase()}`))
                    .map(t => t.replace("filter-", ""));

                return {
                    el: el,
                    terms: terms
                }
            })
            // then write to DOM
            .forEach(x => {
                const el = x.el,
                    elTerms = x.terms;

                let show = filters.filter(f => f.checked)
                    .map(f => elTerms.indexOf(f.slug) >= 0)
                    .reduce((acc, cur) => acc && cur, true);

                show = show || isEmpty;
                el.classList[show ? "remove" : "add"]("hidden");
            })
    }

    Array.from(document.getElementsByClassName('filter__input'))
        .forEach(i => i.addEventListener('change', onFilterChange));

    onFilterChange();
})();