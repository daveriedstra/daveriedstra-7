'use strict';

angular.module('driedUtils', [])
	.value('dried', (function() {
		var out = angular.copy(window.dried);
		window.dried = ''; // take dried out of global scope

		Object.keys(out.compositions).forEach(i => {
			var c = out.compositions[i];
			if (c.yearSort)
				c.yearSort = new Date(c.yearSort)
		})
		return out;
	})())
	.filter('orderObjectBy', function() {
		// https://github.com/fmquaglia/ngOrderObjectBy
		return function (items, field, reverse) {

			function isNumeric(n) {
				return !isNaN(parseFloat(n)) && isFinite(n);
			}

			var filtered = [];

			angular.forEach(items, function(item, key) {
				item.key = key;
				filtered.push(item);
			});

			function index(obj, i) {
				return obj[i];
			}

			filtered.sort(function (a, b) {
				var comparator;
				var reducedA = field.split('.').reduce(index, a);
				var reducedB = field.split('.').reduce(index, b);

				if (isNumeric(reducedA) && isNumeric(reducedB)) {
					reducedA = Number(reducedA);
					reducedB = Number(reducedB);
				}

				if (reducedA instanceof Date)
					reducedA = reducedA.valueOf();
				if (reducedB instanceof Date)
					reducedB = reducedB.valueOf();

				if (reducedA === reducedB) {
					comparator = 0;
				} else {
					comparator = reducedA > reducedB ? 1 : -1;
				}

				return comparator;
			});

			if (reverse) {
				filtered.reverse();
			}

			return filtered;
		};
	})
	.service('driedUtils', ['$http', 'dried', function driedUtils($http, dried) {
		/**
		*		Convenience function for making AJAX calls to the WP server
		*		
		*		postData needs an action attribute
		*		returns a Promise
		*/
		this.ajax = function ajax(postData) {
			return new Promise(function(resolve, reject) {
				if (typeof postData != 'object') 
					return reject( new Error('driedUtils.ajax() requires object to be passed, got ' + (typeof postData)) );
				if (!('action' in postData))
					return reject( new Error ('driedUtils.ajax() needs action') );

				postData.ajax_nonce = dried.ajax_nonce;
				
				// clean postData
				for (var i in postData) {
					if (postData[i] == undefined) {
						delete postData[i];
					} else if (typeof postData[i] == 'object') {
						postData[i] = JSON.stringify(postData[i]);
					}
				}

				$http({
					url: dried.ajax_url,
					method: 'POST',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					transformRequest: function(obj) {
						var str = [];
						for (var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
					},
					data: postData
				})
				.then(function _resolve(response) {
					// check for errors
					if (response.status == 200) {
						if (response.data.success) {
							resolve(response.data);
						} else {
							console.dir(response);
							reject(new Error(response.data.message || 'Unknown error'));
						}
					}
				}, reject);
			});
		}
	}])