'use strict';
angular.module('dried', ['driedUtils', 'ang-drag-drop'])
	.controller('CompositionsPage', ['$scope', 'dried', 'driedUtils', function($scope, dried, driedUtils) {
		var compositions = $scope.compositions = dried.compositions;
		var groups = $scope.groups = dried.groups;
		$scope.stagedWork = {};
		$scope.stagedGroup = {};
		$scope.ajaxStatus = '';
		var mediaFrame, mediaObject, mediaMainID = wp.media.model.settings.post.id;

		/**
		*		shows the media picker
		*
		*		obj is an object which holds the current media at prop
		*		such that obj[prop] == currentMediaID
		*
		*/
		$scope.showMediaPicker = function showMediaPicker(obj, prop, save) {
			mediaObject = {
				id : obj[prop],
				obj : obj,
				prop : prop,
				save : !!save
			}

			// if the frame already exists, set the post and open it
			if (mediaFrame) {
				mediaFrame.uploader.uploader.param('post_id', mediaObject.id);
				mediaFrame.open();
				return;
			}

			// create the frame
			wp.media.model.settings.post.id = mediaObject.id;
			mediaFrame = wp.media.frames.file_frame = wp.media({
				title: 'Select a file',
				button: {
					text: 'Use this file',
				},
				multiple: false	// Set to true to allow multiple files to be selected
			});

			// set event handlers
			mediaFrame.on('select', () => {
				// get first file from uploader
				var attachment = mediaFrame.state().get('selection').first();
				if (attachment)
					attachment = attachment.toJSON();
				else 
					return console.warn('No attachment selected...');
				
				$scope.$evalAsync(() => {
					// save attachment to local model
					mediaObject.obj[mediaObject.prop] = attachment.id;

					// maybe save local model to db
					if (mediaObject.save) saveCompositions();

					// restore "main ID" (what even is this?)
					wp.media.model.settings.post.id = mediaMainID;
				});
			});

			// open new frame
			mediaFrame.open();
		}

		/**
		*		Adds the currently staged group to the groups list
		*	
		*/
		$scope.addGroup = function addGroup() {
			// add new work
			var newGroup = angular.copy($scope.stagedGroup);
			if (!newGroup.slug) {
				newGroup.slug = newGroup.name.replace(/\s/g, "_");
				newGroup.slug += `${Math.round(Math.random() * 1000)}`;
			}
			groups[newGroup.slug] = newGroup;
			$scope.stagedGroup = {}; // clear stage

			// save new data to server
			saveGroups();
		}

		$scope.stageGroup = function stageGroup(g) {
			$scope.stagedGroup = angular.copy(g);
		}

		$scope.deleteGroup = function deleteGroup(g) {
			delete groups[g.slug];
			saveGroups();

			// remove group from compositions
			Object.keys(compositions).forEach(i => {
				if (compositions[i].group == g.slug)
					delete compositions[i].group;
			})
			saveCompositions();
		}

		/**
		*		Adds the currently staged work to the composition list
		*	
		*/
		$scope.addWork = function addWork() {
			cleanCompositions(); // make sure is object

			// add new work
			var newWork = angular.copy($scope.stagedWork);
			newWork.name = newWork.name.replace(/\s/g, '_');
			newWork.i = Object.keys(compositions).length;
			compositions[newWork.name] = newWork;
			$scope.stagedWork = {}; // clear stage

			// save new data to server
			saveCompositions();
		}

		/**
		*		Delete the work and save
		*
		*/
		$scope.deleteWork = function deleteWork(i) {
			compositions[i] = undefined;
			delete compositions[i];
			saveCompositions();
		}

		/**
		*		Makes sure the compositions object is actually an object
		*
		*/
		var cleanCompositions = function cleanCompositions() {
			if (Array.isArray(compositions)) {
				var newComps = {};
				for (var i in compositions) {
					if ('name' in compositions[i]) {
						compositions[i].name = compositions[i].name.replace(/\s/g, '_');
						newComps[compositions[i].name] = angular.copy(compositions[i]);
					}  else {
						console.warn('This composition has no name or an invalid name:');
						console.dir(compositions[i]);
					}
				}
				compositions = newComps;
				console.log('compositions updated from Array to Object');
			}
		}

		/**
		 * converts in.yearSort Date to timestamp (ms)
		 */
		var convertYearSort = function convertYearSort(input) {
			let out = angular.copy(input);
			if (out.yearSort instanceof Date)
				out.yearSort = out.yearSort.valueOf();
			return out;
		}

		/**
		*		Saves the compositions data to the db
		*
		*/
		var saveCompositions = $scope.saveCompositions = function saveCompositions() {
			$scope.ajaxStatus = '...';

			// make sure we don't muck up the working copy
			let saveObj = angular.copy(compositions);

			// convert yearSort to ms
			Object.keys(saveObj).forEach(i => {
				saveObj[i] = convertYearSort(saveObj[i]);
			})

			driedUtils.ajax({
				action : 'save_compositions',
				works : JSON.stringify(saveObj)
			}).then(
				() => $scope.$evalAsync(() => $scope.ajaxStatus = 'Saved.' ),
				(err) => $scope.$evalAsync(() => $scope.ajaxStatus = err.message)
			);
		}

		/**
		 * saves groups
		 */
		var saveGroups = $scope.saveGroups = function saveGroups() {
			$scope.ajaxStatus = '...';

			driedUtils.ajax({
				action : 'save_groups',
				groups : JSON.stringify(groups)
			}).then(
				() => $scope.$evalAsync(() => $scope.ajaxStatus = 'Saved.'),
				(err) => $scope.$evalAsync(() => $scope.ajaxStatus = err.message)
			)
		}
	}]);