<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article <?php post_class('wrap') ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1><?php the_title(); ?></h1>
				
				<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
			</header>

			<div class="entry">
				<?php the_content(); ?>
			</div>
			
			<footer>
				<?php the_tags( 'Tags: ', ', ', ''); ?>
			</footer>
			
		</article>

		<?php //<div class="wrap"> ?>
			<?php //comments_template(); // only if article ?>
		<?php // </div> ?>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>