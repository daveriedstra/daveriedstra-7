<?php get_header(); ?>

	<div class="wrap">
		<h1>404 Page Not Found</h1>
	</div>

	<article class="post wrap">
		<p>Hey. Yikes. That page isn't here – sorry. There are lots of other cool pages though, maybe one of them will have the droids you're looking for.</p>
	</article>
	
<?php get_footer(); ?>