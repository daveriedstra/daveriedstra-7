<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article class="post wrap" id="post-<?php the_ID(); ?>">
		<?php if (get_the_title()): ?>
			<h1 class="post-title"><?php the_title(); ?></h1>
		<?php endif; ?>
			<?php the_content(); ?>
		</article>

		<?php endwhile; endif; ?>

<?php get_footer(); ?>
